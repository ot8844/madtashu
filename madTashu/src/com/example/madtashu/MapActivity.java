package com.example.madtashu;

import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity {
	String tashuInfoURL = "http://210.90.190.21:7425/mapAction.do?process=statusMapView";
	String JsonInfo;
	JSONObject tashuJobj;
	GoogleMap map;
	JSONArray jarray;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		try {
			JsonInfo = new readJsonInfo().execute(tashuInfoURL).get();
			tashuJobj = new JSONObject(JsonInfo);
			jarray = tashuJobj.getJSONArray("markers");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LatLng loc = new LatLng(36.369, 127.388);
		map = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 16));

		// MarkerOptions opt = new MarkerOptions();
		// opt.position(loc);
		// opt.title("marker test 1");
		// opt.snippet("�������� ����");
		// map.addMarker(opt);
		for (int i = 0; i < jarray.length(); i++) {

			try {
				JSONObject tashuStop = jarray.getJSONObject(i);
				MarkerOptions opt = new MarkerOptions();
				
				opt.position(new LatLng(tashuStop.getDouble("lat"), tashuStop
						.getDouble("lng")));
				opt.title(tashuStop.getString("name"));
				if (tashuStop.getInt("cntRentable") != 0) {
					opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.green));
					opt.anchor(0.5f, 1.0f);
				} else {
					opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.red));
					opt.anchor(0.5f, 1.0f);
				}
					

				map.addMarker(opt);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		map.setOnMarkerClickListener(new OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker marker) {
				JSONObject tashuStop = null;
				int rackTotal = 0, rentable = 0;
				String imgUrl = null;
				String stopname = marker.getTitle();
				double lat = marker.getPosition().latitude;
				double lng = marker.getPosition().longitude;
				Log.e("tag", "lat: " + lat);
				for (int i = 0; i < jarray.length(); i++) {
					try {
						tashuStop = jarray.getJSONObject(i);

						if (Math.abs(tashuStop.getDouble("lat") - lat) < 0.000001) {
							rackTotal = tashuStop.getInt("cntRackTotal");
							rentable = tashuStop.getInt("cntRentable");
							imgUrl = "http://www.tashu.or.kr/"
									+ tashuStop.getString("imgFile");
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				Intent i = new Intent(getApplicationContext(),
						StopinfoActivity.class);
				i.putExtra("stopname", stopname);
				i.putExtra("rackTotal", rackTotal );
				i.putExtra("rentable", rentable );
				i.putExtra("imgUrl", imgUrl );
				i.putExtra("lat", lat);
				i.putExtra("lng", lng);
				startActivity(i);
				return false;
			}

		});
	}
	
	
}
