package com.example.madtashu;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class readJsonInfo extends AsyncTask<String, String, String> {

	@Override
	protected String doInBackground(String... urls) {
		// TODO execute메소드로 url을 받아서, JSON string을 build한다?
		HttpClient httpclient = new DefaultHttpClient();
		StringBuilder builder = new StringBuilder();
		HttpPost httppost = new HttpPost(urls[0]);
		try {
			HttpResponse response = httpclient.execute(httppost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Log.e("tag", "builder.tostring : " + builder.toString());
		return builder.toString();
	}

	protected void onPostExecute(String result) {
		super.onPostExecute(result);
	}

}
