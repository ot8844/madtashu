package com.example.madtashu;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.ViewGroup.LayoutParams;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class PreviewActivity extends Activity implements GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener, LocationListener{
	
	
CameraView m_cameraView;
	
	Handler handler = new Handler(){
		 
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.e("handler", "got message" + msg.toString());
        }
 
    }; 

    Location m_last_location = null;
    
    LocationManager m_locationManager;
    
    LocationRequest m_locationRequest;
    LocationClient m_locationClient;
    boolean mUpdatesRequested;
    
    SensorManager m_sensorManager;
    Sensor m_accelerometer;
    //Sensor m_magnetometer;
    
    DrawOnTop m_drawView;
    
    SharedPreferences  mPrefs;
    Editor mEditor;
    
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent i = getIntent();
		//int idx = (Integer)getIntent().getExtras().get("idx");
		String stopname = i.getExtras().getString("stopname");
		Double lat = i.getExtras().getDouble("lat");
		Double lng = i.getExtras().getDouble("lng");
		
		
		m_sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		m_accelerometer = m_sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		
		m_cameraView = new CameraView(this);
        setContentView(m_cameraView);
		
        
     // Open the shared preferences
       mPrefs = getSharedPreferences("SharedPreferences",
                Context.MODE_PRIVATE);
        // Get a SharedPreferences editor
        mEditor = mPrefs.edit();
        /*
         * Create a new location client, using the enclosing class to
         * handle callbacks.
         */ 
        m_locationClient = new LocationClient(this, this, this);
        m_locationClient.connect();
        // Start with updates turned off
        mUpdatesRequested = false;

        m_locationRequest = LocationRequest.create();
		 m_locationRequest.setPriority(
	                LocationRequest.PRIORITY_HIGH_ACCURACY);
	
		 m_locationRequest.setInterval(0);
		 m_locationRequest.setFastestInterval(0);
		 
		 
        /*
		m_locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		

		 m_locationRequest = LocationRequest.create();
		 m_locationRequest.setPriority(
	                LocationRequest.PRIORITY_HIGH_ACCURACY);
		 m_locationRequest.setInterval(3000);
		 m_locationRequest.setFastestInterval(500);
		
		Criteria cri = new Criteria();
		cri.setPowerRequirement(Criteria.POWER_HIGH);
		cri.setAccuracy(Criteria.ACCURACY_FINE);
		cri.setBearingRequired(false);
		cri.setAltitudeRequired(false);
		cri.setCostAllowed(false);
		
		String provider = m_locationManager.getBestProvider(cri, true);
		Log.e("provider", provider);
		m_last_location = m_locationManager.getLastKnownLocation(provider);
		m_locationManager.requestLocationUpdates(provider, 0, 0, this);
		*/
		
        m_drawView = new DrawOnTop(this); 
        //idx를 여기서 집어넣는다.
        m_drawView.stopname = stopname;
        m_drawView.lat = lat;
        m_drawView.lng = lng;

        addContentView(m_drawView, new LayoutParams 
        (LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)); 

	}
	
	@Override
    protected void onStop() {
        // If the client is connected
        if (m_locationClient.isConnected()) {
            /*
             * Remove location updates for a listener.
             * The current Activity is the listener, so
             * the argument is "this".
             */
            m_locationClient.removeLocationUpdates(this);
        }
        /*
         * After disconnect() is called, the client is
         * considered "dead".
         */
        m_locationClient.disconnect();
        super.onStop();
    }
	
	@Override
	protected void onDestroy(){
		Log.e("destroy", "destroy");
		super.onDestroy();
		//m_locationManager.removeUpdates(this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
    protected void onResume(){
    	super.onResume();
    	m_sensorManager.registerListener(m_drawView, m_accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    	
    	if (mPrefs.contains("KEY_UPDATES_ON")) {
            mUpdatesRequested =
                    mPrefs.getBoolean("KEY_UPDATES_ON", false);

        // Otherwise, turn off location updates
        } else {
            mEditor.putBoolean("KEY_UPDATES_ON", false);
            mEditor.commit();
        }
    	
    }
    @Override
    protected void onPause(){
    	
    	super.onPause();
    	m_sensorManager.unregisterListener(m_drawView);
    	
    	// Save the current setting for updates
        mEditor.putBoolean("KEY_UPDATES_ON", mUpdatesRequested);
        mEditor.commit();
        super.onPause();
    }
    
    @Override
	public void onLocationChanged(Location arg0) {
		Log.e("location manager", "location changed");
		//tv.setText("location changed  " + arg0.getLatitude() +  " " + arg0.getLongitude());
		
		m_last_location = arg0;
		m_drawView.invalidate();
		//m_drawView.postInvalidate();
		//Toast.makeText(this, m_last_location.getLatitude() + " : " + m_last_location.getLongitude(), Toast.LENGTH_LONG).show();
		//Log.e("loc", m_last_location.getLatitude() + " : " + m_last_location.getLongitude());
		
	}

    /*
	@Override
	public void onProviderDisabled(String arg0) {
		Log.e("location manager", "provider disabled : " + arg0);
		//tv.setText("location disabled : " + arg0);
		
		Toast toast = Toast.makeText(getApplicationContext(),
				   "gps를 켜주세요", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				
	}

	@Override
	public void onProviderEnabled(String arg0) {
		Log.e("location manager", "provider enabled : " + arg0);
		//tv.setText("location enabled : " + arg0);
		
		Toast toast = Toast.makeText(getApplicationContext(),
				   "gps 사용 가능", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		Log.e("location manager", "status changed" + arg0);
		//tv.setText("status changed : " + arg0);
	}
	*/

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		 if (result.hasResolution()) {
	            try {
	                // Start an Activity that tries to resolve the error
	            	result.startResolutionForResult(
	                        this,
	                        9000);
	                /*
	                * Thrown if Google Play services canceled the original
	                * PendingIntent
	                */
	            } catch (IntentSender.SendIntentException e) {
	                // Log the error
	                e.printStackTrace();
	            }
	        } else {
	            /*
	             * If no resolution is available, display a dialog to the
	             * user with the error.
	             */
	        	
	        	Log.e("connection failed", result.getErrorCode() + "");
	            //showErrorDialog(result.getErrorCode());
	        }
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		// TODO Auto-generated method stub
		Log.e("gps", "onconnected");
		//if (mUpdatesRequested) {
			//Toast.makeText(this, "location service started", Toast.LENGTH_LONG).show();
            m_locationClient.requestLocationUpdates(m_locationRequest, this);
       // }
		
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		Log.e("a", "ondisconnected");
	}
    
}