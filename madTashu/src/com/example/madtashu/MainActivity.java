package com.example.madtashu;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.example.madtashu.LocalService.LocalBinder;
import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends TabActivity {
	LocalService mService;
	boolean mBound = false;
	int cnt;
	private Handler mHandler;
	boolean buttonClick;

	DB _db;
	SQLiteDatabase db;
	Runnable r;

	TextView textTime;
	TextView textDistance;
	TextView textVelocity;
	TextView textKcal;

	ListView listView;

	ArrayList<String> list;
	ListAdapter adapter;

	/* new value */
	int weight;
	double kcal;
	double preTime;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mBound = false;
		mHandler = new Handler();
		cnt = 0;
		kcal = 0;
		preTime = 0;
		weight = 50;

		_db = new DB(this);

		db = _db.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM dbInfo WHERE _id = 1", null);
		c.moveToFirst();
		if (c.getInt(1) == 0) {
			buttonClick = false;
		} else {
			buttonClick = true;
		}
		db.close();

		setContentView(R.layout.activity_main);
		makeTab();
		tab2func();
		tab3func();

		r = new Runnable() {
			@Override
			public void run() {
				if (mBound) {
					//System.out.println("bsafgsfagshfah");
					cnt++;
					if (mService.getLastProvider().equals(
							LocationManager.GPS_PROVIDER)) {
						textTime.setText((new Integer(cnt)).toString());
						Double d = new Double(Double.parseDouble(String.format(
								"%.3f", mService.getDistance())));
						textDistance.setText(d.toString());
						Double v = new Double(Double.parseDouble(String.format(
								"%.3f", mService.getVelocity())));
						textVelocity.setText(v.toString());

						double now = mService.getEndTime();
						Double k = new Double(Double.parseDouble(String.format(
								"%.3f", mService.getKcal()/1000)));
						textKcal.setText(k.toString());
						preTime = now;
					} else {
						textTime.setText((new Integer(cnt)).toString());
						Double d = new Double(Double.parseDouble(String.format(
								"%.3f", mService.getDistance())));
						textDistance.setText(d.toString());
						Double v = new Double(Double.parseDouble(String.format(
								"%.3f", mService.getVelocity())));
						textVelocity.setText(v.toString());

						double now = mService.getEndTime();
						Double k = new Double(Double.parseDouble(String.format(
								"%.3f", mService.getKcal()/1000)));
						textKcal.setText(k.toString());
						preTime = now;
					}

				}
				if (mBound)
					mHandler.postDelayed(this, 1000);
			}
		};
		if (buttonClick) {
			if (!mBound) {
				Intent intent = new Intent(MainActivity.this,
						LocalService.class);
				bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
				mBound = true;
			}
			mHandler.postDelayed(r, 1000);
		}
	}

	private void makeTab() {
		TabHost tabHost = (TabHost) getTabHost();
		tabHost.setup();
		
		// Tab1
		

		Intent intent = new Intent().setClass(this, MapActivity.class);
		// spec1.setContent(intent);
		 TabSpec spec1 = tabHost.newTabSpec("Tab1");
		 spec1.setContent(R.id.tab1);
		//TabSpec spec1 = tabHost.newTabSpec("Tab1").setContent(intent);
		spec1.setIndicator("",
				getResources().getDrawable(R.drawable.tab2));
		tabHost.addTab(spec1);

		// Tab2
		TabSpec spec2 = tabHost.newTabSpec("Tab2");
		spec2.setContent(R.id.tab2);
		spec2.setIndicator("",
				getResources().getDrawable(R.drawable.tab1));
		tabHost.addTab(spec2);
		// Tab3
		TabSpec spec3 = tabHost.newTabSpec("Tab3");
		spec3.setContent(R.id.tab3);
		spec3.setIndicator("",
				getResources().getDrawable(R.drawable.tab3));
		tabHost.addTab(spec3);

		
		for(int i=0;i<3;i++){
			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#BBBBBB"));
		}

		tabHost.getTabWidget().getChildAt(0).getLayoutParams().height = 120;
		tabHost.getTabWidget().getChildAt(1).getLayoutParams().height = 120;
		tabHost.getTabWidget().getChildAt(2).getLayoutParams().height = 120;

		tabHost.setCurrentTab(0);

	}

	private void tab2func() {
		Button button = (Button) findViewById(R.id.tashuStart);
		if (buttonClick) {
			button.setBackgroundResource(R.drawable.tashu_running);
		} else {
			button.setBackgroundResource(R.drawable.tashu);
		}

		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (buttonClick) {
					db = _db.getWritableDatabase();
					db.execSQL("UPDATE dbInfo set tashuOn=0 where _id=1");
					db.close();
					listAdd(arg0.getContext());

					if (mBound) {
						unbindService(mConnection);
						mBound = false;
					}
					if (buttonClick) {
						stopService(new Intent(MainActivity.this,
								LocalService.class));
						buttonClick = false;
					}

					arg0.setBackgroundResource(R.drawable.tashu);
					return;
				}
				db = _db.getWritableDatabase();
				db.execSQL("UPDATE dbInfo set tashuOn=1 where _id=1");
				db.close();
				buttonClick = true;
				arg0.setBackgroundResource(R.drawable.tashu_running);

				/*
				 * AlertDialog.Builder alertDlg = new
				 * AlertDialog.Builder(arg0.getContext());
				 * alertDlg.setTitle(R.string.alert_title_question); final
				 * EditText input = new EditText(arg0.getContext());
				 * alertDlg.setView(input);
				 * 
				 * alertDlg.setPositiveButton( R.string.button_yes, new
				 * DialogInterface.OnClickListener() {
				 * 
				 * @Override public void onClick( DialogInterface dialog, int
				 * which ) { weight =
				 * Integer.parseInt(input.getText().toString());
				 * dialog.dismiss(); } });
				 * 
				 * alertDlg.show();
				 */
				if (!mBound) {
					startService(new Intent(MainActivity.this,
							LocalService.class));
					Intent intent = new Intent(MainActivity.this,
							LocalService.class);
					bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
					mBound = true;

					mHandler.postDelayed(r, 1000);

				}
			}
		});

		textTime = (TextView) findViewById(R.id.timeText);
		textDistance = (TextView) findViewById(R.id.distanceText);
		textVelocity = (TextView) findViewById(R.id.velocityText);
		textKcal = (TextView) findViewById(R.id.kcalText);

		return;
	}

	private void tab3func() {

		list = new ArrayList<String>();
		File file = new File("data/data/com.example.madtashu/files/");
		if (file != null) {
			File[] fileList = file.listFiles();

			if (fileList != null) {
				for (int i = 0; i < fileList.length; i++) {
					String temp = fileList[i].getName();
					System.out.println(temp);
					if (temp.length() >= 5
							&& temp.substring(temp.length() - 5, temp.length())
									.equals(".json")) {
						list.add(0, temp.substring(0, temp.length() - 5));
					}
				}
			}
		}
		listView = (ListView) findViewById(R.id.list);
		adapter = new ListAdapter(getApplicationContext(), list, _db);
		listView.setAdapter(adapter);

		listView.setOnItemLongClickListener(new OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int arg2, long arg3) {
				AlertDialog.Builder alertDlg = new AlertDialog.Builder(
						MainActivity.this);
				alertDlg.setTitle(R.string.alert_title_question3);
				alertDlg.setPositiveButton(R.string.button_yes,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								File file = new File(
										"data/data/com.example.hs/files/"
												+ list.get(arg2) + ".json");
								file.delete();
								list.remove(arg2);
								adapter.notifyDataSetChanged();
								dialog.dismiss(); // AlertDialog�� �ݴ´�.
							}
						});
				alertDlg.setNegativeButton(R.string.button_no,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss(); // AlertDialog�� �ݴ´�.
							}
						});
				alertDlg.show();
				return true;
			}
		});

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(MainActivity.this,
						RunViewActivity.class);
				intent.putExtra("name", list.get(position));
				// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);

			}
		});

	}

	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// We've bound to LocalService, cast the IBinder and get
			// LocalService instance
			LocalBinder binder = (LocalBinder) service;
			mService = binder.getService();

		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
		}
	};

	@Override
	protected void onStop() {
		super.onStop();
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		if (buttonClick) {
			if (!mBound) {
				Intent intent = new Intent(MainActivity.this,
						LocalService.class);
				bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
				mBound = true;
			}
			mHandler.postDelayed(r, 1000);
			Log.i("zzz", "33");
		}

	}

	private void listAdd(Context context) {
		AlertDialog.Builder alertDlg = new AlertDialog.Builder(context);
		alertDlg.setTitle(R.string.alert_title_question2);
		final EditText input = new EditText(context);
		alertDlg.setView(input);

		alertDlg.setPositiveButton(R.string.button_yes,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(
								"yyyy.MM.dd HH:mm:ss", Locale.KOREA);
						Date currentTime = new Date();
						String mTime = mSimpleDateFormat.format(currentTime);

						list.add(0, input.getText().toString() + mTime);
						create_file(input.getText().toString() + mTime, dialog);
						adapter.notifyDataSetChanged();
						dialog.dismiss();
					}
				});
		alertDlg.setNegativeButton(R.string.button_no,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		alertDlg.show();
		return;
	}

	private void create_file(String file_name, DialogInterface dialog) {
		JSONObject temp = new JSONObject();
		JSONArray ja = new JSONArray();
		JSONArray ja2 = new JSONArray();

		ArrayList<LatLng> t = mService.getLaLo();

		try {
			for (int i = 0; i < t.size(); i++) {
				ja.put(t.get(i).latitude);
				ja2.put(t.get(i).longitude);
			}

			temp.put("startTime", mService.getStartTime());
			temp.put("endTime", mService.getEndTime());
			temp.put("la", ja);
			temp.put("lo", ja2);
			temp.put("kcal", mService.getKcal());
			temp.put("dis", mService.getDistance() + mService.getPreDistance());
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		FileOutputStream f;
		try {
			f = openFileOutput(file_name + ".json",
					getApplicationContext().MODE_PRIVATE);
			f.write(temp.toString().getBytes());
			f.flush();
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return;
	}

}