package com.example.madtashu;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

public class RunViewActivity extends FragmentActivity {
	GoogleMap map;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_runview);
		
		map = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		
		ArrayList<LatLng> list=new ArrayList<LatLng>();
		
		
		Intent intent = getIntent();
		double calories = 0,speed = 0,distance = 0,totaltime = 0;
		
		FileInputStream ais;
		try {
			ais = new FileInputStream("data/data/com.example.madtashu/files/"+ intent.getStringExtra("name") + ".json");
			BufferedReader br = new BufferedReader(new InputStreamReader(ais));
			StringBuilder sb = new StringBuilder();
			int bufferSize = 1024 * 1024;
			char readBuf[] = new char[bufferSize];
			int resultSize = 0;
			while ((resultSize = br.read(readBuf)) != -1) {
				if (resultSize == bufferSize) {
					sb.append(readBuf);
				} else {
					for (int i = 0; i < resultSize; i++) {
						sb.append(readBuf[i]);
					}
				}
			}
			String jString = sb.toString();
			JSONObject jo = new JSONObject(jString);
			
			System.out.println(jString);
			
			distance = jo.getDouble("dis");
			calories = jo.getDouble("kcal");
			totaltime = (jo.getDouble("endTime") - jo.getDouble("startTime"))/1000;
			
			System.out.println("==================");
			System.out.println(jo.getDouble("startTime"));
			System.out.println(jo.getDouble("endTime"));
			System.out.println("==================");
			
			speed = distance/totaltime;
			
			JSONArray la = jo.getJSONArray("la");
			JSONArray lo = jo.getJSONArray("lo");
			
			for (int i=0; i<la.length(); i++){
				list.add(new LatLng(la.getDouble(i), lo.getDouble(i)));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		double latsum=0, lngsum=0;
		for (int i=0;i<list.size();i++)
		{
			latsum+=list.get(i).latitude;
			lngsum+=list.get(i).longitude;
		}
		
		LatLng center = new LatLng(latsum/list.size(), lngsum/list.size());
		//ArrayList<LatLng> list = (ArrayList<LatLng>) getIntent().getSerializableExtra("locs");
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(center , 16));
		map.getUiSettings().setScrollGesturesEnabled(false);
		PolylineOptions po = new PolylineOptions().addAll(list);
		po.color(Color.RED);
		map.addPolyline(po);
		
		//칼로리,속력, 거리, 총 시간 입력받을것
		TextView tv1 = (TextView) findViewById(R.id.tv1);
		TextView tv2 = (TextView) findViewById(R.id.tv2);
		TextView tv3 = (TextView) findViewById(R.id.tv3);
		TextView tv4 = (TextView) findViewById(R.id.tv4);
		Double t = new Double(Double.parseDouble(String.format("%.3f", distance)));

		tv1.setText(" 거리 : "+t+" m");
		t = new Double(Double.parseDouble(String.format("%.3f", totaltime/60)));
		tv2.setText(" 시간 : "+t+" 분");
		t = new Double(Double.parseDouble(String.format("%.3f", speed)));
		tv3.setText(" 평균 속력 : "+t+" m/s");
		t = new Double(Double.parseDouble(String.format("%.3f", calories/1000)));
		tv4.setText(" 소모 칼로리 : "+t+" kcal");
		
	}
}