package com.example.madtashu;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class CameraView extends SurfaceView implements SurfaceHolder.Callback{

	 SurfaceHolder mHolder;
	 Camera mCamera;
	    
	public CameraView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		// SurfaceHolder.Callback을 설정함으로써 Surface가 생성/소멸되었음을
        
		// 알 수 있습니다.
		mHolder = getHolder();
		mHolder.addCallback(this);
		//mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// TODO Auto-generated method stub
		// 표시할 영역의 크기를 알았으므로 해당 크기로 Preview를 시작합니다.
        
        Camera.Parameters parameters = mCamera.getParameters();
        List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();

        // You need to choose the most appropriate previewSize for your app
        Camera.Size previewSize = parameters.getPreferredPreviewSizeForVideo();// previewSizes.get(previewSizes.size()-1);
        
        parameters.setPreviewSize(previewSize.width, previewSize.height);
        
        
        mCamera.setParameters(parameters);
        mCamera.setDisplayOrientation(90);
        mCamera.startPreview();
        
        Log.e("camera view", "surface changed");
        
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		
		// Surface가 생성되었다면, 카메라의 인스턴스를 받아온 후 카메라의
        
		// Preview 를 표시할 위치를 설정합니다.
		        mCamera = Camera.open();
		        try {
		           mCamera.setPreviewDisplay(arg0);
		        } catch (IOException exception) {
		            mCamera.release();
		            mCamera = null;
		            Log.e("camera", exception.toString());
		// TODO: add more exception handling logic here
		        }
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		// 다른 화면으로 돌아가면, Surface가 소멸됩니다. 따라서 카메라의 Preview도 
        
		// 중지해야 합니다. 카메라는 공유할 수 있는 자원이 아니기에, 사용하지 않을
		        
		// 경우 -액티비티가 일시정지 상태가 된 경우 등 - 자원을 반환해야합니다.
		        mCamera.stopPreview();
		        mCamera.release();
		        mCamera = null;
	}
	

}
