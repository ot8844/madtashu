package com.example.madtashu;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DB extends SQLiteOpenHelper{

	public DB(Context context) {
		super(context, "db", null, 4);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE dbInfo" + "("
				+ " _id integer PRIMARY KEY autoincrement,"	
				+ " tashuOn integer)"
				    );
		
		db.execSQL("INSERT INTO dbInfo VALUES (null, 0)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		System.out.println("aaaaaa");
		db.execSQL("DROP TABLE IF EXISTS dbInfo");
		onCreate(db);
	}

}
