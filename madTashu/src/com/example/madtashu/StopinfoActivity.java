package com.example.madtashu;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class StopinfoActivity extends Activity {
	ImageView iv;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*
		 * requestWindowFeature(Window.FEATURE_NO_TITLE);
		 * getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		 * getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
		 * WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
		 */

		WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
		layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		layoutParams.dimAmount = 0.8f; // 투명도 0 ~ 1
		getWindow().setAttributes(layoutParams);

		setContentView(R.layout.stopinfo);

		Intent i = getIntent();
		int rackTotal = i.getExtras().getInt("rackTotal");
		int rentable = i.getExtras().getInt("rentable");
		String imgUrl = i.getExtras().getString("imgUrl");
		final String stopname = i.getExtras().getString("stopname");
		final Double lat = i.getExtras().getDouble("lat");
		final Double lng = i.getExtras().getDouble("lng");
		
	
		iv = (ImageView) findViewById(R.id.stopimg);
		TextView tv1 = (TextView) findViewById(R.id.stopname);
		TextView tv2 = (TextView) findViewById(R.id.racktotal);
		TextView tv3 = (TextView) findViewById(R.id.rentable);
		
		tv1.setText(" 정거장 이름 : "+stopname);
		tv1.setTextSize(20);		
		
		tv2.setText(" 거치대  : 총" + rackTotal +" 대");
		tv2.setTextSize(20);
		
		tv3.setText(" 대여가능 자전거 : "+rentable+" 대");
		tv3.setTextSize(20);
		
		Log.e("tag", "imgurl is " + imgUrl);
		new back().execute(imgUrl);
		
		
		LinearLayout layout = (LinearLayout) findViewById(R.id.linearlayout);
		layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				// TODO Auto-generated method stub
//				Intent cameraActivity=new Intent (StopinfoActivity.this, PreviewActivity.class);
//				cameraActivity.putExtra("stopname", stopname);
//				cameraActivity.putExtra("lat", lat);
//				cameraActivity.putExtra("lng", lng);
//				Log.e("tag", stopname + ""+lat+", "+lng);
//				//printList.putExtra("key",name);
//				startActivity(cameraActivity);
			}
		});
		findViewById(R.id.ar).setOnClickListener(new OnClickListener() {
			public void onClick(View v){
				Intent cameraActivity=new Intent (StopinfoActivity.this, PreviewActivity.class);
				cameraActivity.putExtra("stopname", stopname);
				cameraActivity.putExtra("lat", lat);
				cameraActivity.putExtra("lng", lng);
				Log.e("tag", stopname + ""+lat+", "+lng);
				//printList.putExtra("key",name);
				startActivity(cameraActivity);
			}
		});
	}

	private class back extends AsyncTask<String, Integer, Bitmap> {
		Bitmap bmImg;

		@Override
		protected Bitmap doInBackground(String... urls) {
			try {
				URL myFileUrl = new URL(urls[0]);
				HttpURLConnection conn = (HttpURLConnection) myFileUrl
						.openConnection();
				conn.setDoInput(true);
				conn.connect();

				InputStream is = conn.getInputStream();

				bmImg = BitmapFactory.decodeStream(is);

			} catch (IOException e) {
				e.printStackTrace();
			}
			return bmImg;
		}
		
		protected void onPostExecute(Bitmap img) {
			iv.setImageBitmap(bmImg);
		}
	}
}
