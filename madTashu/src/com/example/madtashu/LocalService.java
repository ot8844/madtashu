package com.example.madtashu;

import java.util.ArrayList;
import java.util.Random;

import com.google.android.gms.maps.model.LatLng;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;

public class LocalService extends Service {
    private final IBinder mBinder = new LocalBinder();
    private final Random mGenerator = new Random();
    private MediaPlayer player;
    
   private LocationManager mLocationManager =null;
   private static final int LOCATION_INTERVAL = 500;
   private static final float LOCATION_DISTANCE = 0;
   
   private String lastProvider;
   private int temp;
   
   private String gpsLatitude;
   private String gpsLongtitude;
   
   private ArrayList<LatLng> laLo;
   private double endTime;

   private String networkLatitude;
   private String networkLongtitude;
   
   private double velocity;   
   private double distance;
   private double preLa;
   private double preLo;
   private double preTime;
   private double preDistance;
   private double startTime;
   
   int cnt;
   Thread a;
   boolean stop;
   boolean activityCall;
   
   
   double kcal;
   
   
   public double getKcal() {
      return kcal;
   }
   public void setKcal(double kcal) {
      this.kcal = kcal;
   }
   public double getVelocity() {
      return velocity;
   }
   public void setVelocity(double velocity) {
      this.velocity = velocity;
   }
   public String getGpsLatitude() {
      return gpsLatitude;
   }
   public void setGpsLatitude(String gpsLatitude) {
      this.gpsLatitude = gpsLatitude;
   }
   public String getGpsLongtitude() {
      return gpsLongtitude;
   }
   public void setGpsLongtitude(String gpsLongtitude) {
      this.gpsLongtitude = gpsLongtitude;
   }
   public String getNetworkLatitude() {
      return networkLatitude;
   }
   public void setNetworkLatitude(String networkLatitude) {
      this.networkLatitude = networkLatitude;
   }
   public String getNetworkLongtitude() {
      return networkLongtitude;
   }
   public void setNetworkLongtitude(String networkLongtitude) {
      this.networkLongtitude = networkLongtitude;
   }
   
   public double getDistance() {
      return distance;
   }
   public void setDistance(double distance) {
      this.distance = distance;
   }
   public String getLastProvider() {
      return lastProvider;
   }
   public void setLastProvider(String lastProvider) {
      this.lastProvider = lastProvider;
   }
   
   int getTemp(){
      return temp;
   }
   
   
   public ArrayList<LatLng> getLaLo() {
      return laLo;
   }
   public void setLaLo(ArrayList<LatLng> laLo) {
      this.laLo = laLo;
   }
   public double getEndTime() {
      return endTime;
   }
   public void setEndTime(double endTime) {
      this.endTime = endTime;
   }
   public double getStartTime() {
      return startTime;
   }
   public void setStartTime(double startTime) {
      this.startTime = startTime;
   }
   public boolean isActivityCall() {
      return activityCall;
   }
   public void setActivityCall(boolean activityCall) {
      this.activityCall = activityCall;
   }


   public double getPreDistance() {
      return preDistance;
   }
   public void setPreDistance(double preDistance) {
      this.preDistance = preDistance;
   }



   Runnable mRun = new Runnable() {       
      public void run() {
         player.start();
         System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaa");
         
         try {
             Thread.sleep(10000);
         }catch(InterruptedException e){
             System.out.println(e.getMessage()); //sleep 메소드가 발생하는 InterruptedException 
         }
         
         if (!stop){

             Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);         
            vibe.vibrate(1000);   
            Context context = getApplicationContext();
            Intent intent2 = new Intent(context, CallActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.putExtra("aa", "bb");
            context.startActivity(intent2);
         }
      }
      
   };
   
   @Override
   public void onCreate(){
      temp = 0;
      velocity=0;
      distance=0;
      preLa = 0;
      preLo = 0;
      cnt = 0;
      preDistance = 0;
      startTime = 0;
      lastProvider=new String ("null!!!");
      gpsLatitude = "";
      gpsLongtitude = "";
      networkLatitude = "";
      networkLongtitude = "";
      stop = false;
      activityCall = false;
      kcal = 0;
      
      
      laLo = new ArrayList<LatLng>();
      
   // 서비스가 처음 실행되면 음악 재생기를 생성합니다.
      System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaas");
      player = MediaPlayer.create(this, R.raw.music);
      player.setLooping(false);
      
      initializeLocationManager();
      try{
         mLocationManager.requestLocationUpdates(
               LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
               mLocationListeners[1]);   
         //push LATITUDE and LONGTITUDE in DB
      } catch (java.lang.SecurityException ex){
         System.out.println("catch");
      }catch (IllegalArgumentException ex){
         System.out.println("catch2");
      }
      
      try{
         mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, 
               mLocationListeners[0]);
      } catch (java.lang.SecurityException ex){
         System.out.println("catch3");
      }catch (IllegalArgumentException ex){
         System.out.println("catch4");
      }
      
      System.out.println("create");
      
   }
   
    public class LocalBinder extends Binder {
        LocalService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LocalService.this;
        }
    }
 
    @Override
    public IBinder onBind(Intent intent) {
       
        return mBinder;
    }
    public int onStartCommand(Intent intent, int flags, int startId){
       // 서비스가 시작될 때 마다 음악 재생을 시작합니다.
      a = new Thread(mRun);
      a.start();

      return super.onStartCommand(intent, flags, startId);
   }
 
    /** method for clients */
    public int getRandomNumber() {
      return mGenerator.nextInt(100);
    }
    
    @Override
   public void onDestroy(){
   // 서비스가 중지되면 음악 재생기를 중지합니다.
      player.stop();
      stop = true;
      mLocationManager.removeUpdates(mLocationListeners[0]);
      mLocationManager.removeUpdates(mLocationListeners[1]);
   }
    
    
    
    private class LocationListener implements android.location.LocationListener{
       Location mLastLocation;
       public LocationListener(String provider)
       {
          mLastLocation = new Location(provider);
          
       }
       public void onLocationChanged(Location location){
          mLastLocation.set(location);
          double LATITUDE =location.getLatitude();
          double LONGTITUDE = location.getLongitude();
          String _latitude =String.valueOf(LATITUDE);
          String _longtitude = String.valueOf(LONGTITUDE);
          System.out.println("ababadbagqrfqwrqwwqe");
       //   System.out.println("LocationChanged "+ location.toString()+ " " + Latitude+" "+Longtitude); 
          lastProvider = location.getProvider();
          if (location.getProvider().equals(LocationManager.GPS_PROVIDER)){
             gpsLatitude = _latitude;
             gpsLongtitude = _longtitude;
             cnt ++;
             if (cnt== 1) preTime = System.currentTimeMillis();
             
             if (preLa == 0){
                preLa = LATITUDE;
                preLo = LONGTITUDE;
             } 
             else{
                double d;
                double now = System.currentTimeMillis();
                Location locationA = new Location("point A");
                locationA.setLatitude(preLa);
                locationA.setLongitude(preLo);
                
                Location locationB = new Location("point B");
                locationB.setLatitude(LATITUDE);
                locationB.setLongitude(LONGTITUDE);
                d = locationA.distanceTo(locationB);
                preDistance += Math.abs(d);
                if (cnt == 5){
                
                   distance +=Math.abs(preDistance);
                   velocity = Math.abs(preDistance)*1000 / (now - preTime);
                   kcal += calkcal(velocity, now - preTime, 60);
                   preLa = LATITUDE;
                   preLo = LONGTITUDE;
                   preTime = now;
                   cnt = 0;
                   preDistance = 0;
                }
                endTime = now;
             }
             if (startTime == 0) startTime = preTime;
             
             laLo.add(new LatLng(LATITUDE, LONGTITUDE));
             
          }else{
             networkLatitude = _latitude;
             networkLongtitude = _longtitude;
          }
          temp++;
          
       }
       @Override
       public void onStatusChanged(String provider, int status, Bundle extras) {
          // TODO Auto-generated method stub
          System.out.println("statuschanged");
          
       }
       @Override
       public void onProviderEnabled(String provider) {
          // TODO Auto-generated method stub
          
       }
       @Override
       public void onProviderDisabled(String provider) {
          // TODO Auto-generated method stub
          
       }
    }
    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    
    
    public void initializeLocationManager() {
       if(mLocationManager ==null){
          mLocationManager=(LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
       }
    } 
    
    double calkcal(double speed, double time, int weight){
      speed = speed / 3.6;
      double alpha;
      if (speed < 13){
         alpha = 0.065/13 * speed;
      }else if (speed <= 16){
         alpha = 0.0783;
      }else if (speed <= 19){
         alpha = 0.0939;
      }else if (speed <= 22){
         alpha = 0.113;
      }else if (speed <= 24){
         alpha = 0.124;
      }else if (speed <= 26){
         alpha = 0.136;
      }else if (speed <= 27){
         alpha = 0.149;
      }else if (speed <= 29){
         alpha = 0.163;
      }else if (speed <= 31){
         alpha = 0.179;
      }else if (speed <= 32){
         alpha = 0.196;
      }else if (speed <= 34){
         alpha = 0.215;
      }else if (speed <= 37){
         alpha = 0.259;
      }else if (speed <= 40){
         alpha = 0.311;
      }else{
         alpha = 0.33;
      }
      
      return 60 * speed * alpha * time/60;
   }
}