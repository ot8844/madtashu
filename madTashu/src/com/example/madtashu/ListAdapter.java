package com.example.madtashu;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListAdapter extends ArrayAdapter<String> {
	private Context context;
	private ArrayList<String> list;
	private LayoutInflater inflater;
	private DB db;
	
	public ListAdapter(Context context, ArrayList<String> list, DB db){
		super(context, 0, list);
		this.context = context;
		this.list = list;
		this.inflater = LayoutInflater.from(context);
		this.db = db;
		//Log.i("aa","aa");
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null){
			convertView = inflater.inflate(R.layout.list_row, null);
		}
    	
		String item = getItem(position);
		
		TextView name = (TextView)convertView.findViewById(R.id.name);
		if (item!=null ||item!="") name.setText(item.substring(0, item.length()-19));
		TextView date = (TextView)convertView.findViewById(R.id.date);
		if (item!=null ||item!="") date.setText(item.substring(item.length()-19, item.length()));

		return convertView;
	}
}
