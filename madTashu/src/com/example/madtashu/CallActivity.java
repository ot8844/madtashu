package com.example.madtashu;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;

public class CallActivity extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
         layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
         layoutParams.dimAmount = 0.5f; // ������ 0 ~ 1
         getWindow().setAttributes(layoutParams);
           setContentView(R.layout.activity_call);
       }
}